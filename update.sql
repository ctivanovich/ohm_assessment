USE ohm_assessment;

ALTER TABLE user ADD COLUMN location varchar(100);
ALTER TABLE `rel_user_multi` CHANGE `attribute` `phone_number` varchar(255);
ALTER TABLE `rel_user` change `attribute` `location` varchar(255);

UPDATE user SET point_balance = 1000 WHERE user_id = 1;
UPDATE user SET location = "Chicago, IL" WHERE user_id = 2;
UPDATE user SET tier = "Silver" WHERE user_id = 3;
