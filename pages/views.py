from flask import jsonify, render_template, request, Response
from flask.ext.login import current_user, login_user

from functions import app
from models import User

@app.route('/dashboard', methods=['GET'])
def dashboard():

    login_user(User.query.get(1))

    args = {
            'gift_card_eligible': True,
            'cashout_ok': True,
            'user_below_silver': current_user.is_below_tier('Silver'),
    }
    return render_template("dashboard.html", **args)

@app.route('/community', methods=['GET'])
def community():
    sql = '''
        SELECT *
        FROM user
        ORDER BY signup_date
        LIMIT 5;'''

    results = User.query.from_statement(sql).all()
    columns = [
            u"Full Name",
            u"Point Balance",
            u"Current Tier",
            u"Phone Number(s)",
            ]
    # user_data = []
    # for result in results:
    #     phone = result.get_multi("PHONE")
    #     location = result.get_attribute("LOCATION")
    #     user_data.append({
    #                 "display_name": result.display_name,
    #                 "point_balance": result.point_balance,
    #                 "tier": result.tier,
    #                 "phone_numbers": phone,
    #                 "location": location,
    #                 })
    args = {
            'attributes': columns,
            'user_data': results,
            }

    return render_template("community.html", **args)
